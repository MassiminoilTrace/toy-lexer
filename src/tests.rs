use crate::lexer;

#[test]
fn test_main_empty()
{
    const SOURCE: &'static str = r##"int main()
    {
        return 2;
    } "##;
    let mut l = lexer::Lexer::new();
    l.load_from_str(SOURCE);
}

#[test]
fn test_if_else()
{
    const SOURCE: &'static str = r##"int main()
    {
        if (a==2)
        {
            return 5;
        }
        return 2;
    } "##;
    let mut l = lexer::Lexer::new();
    l.load_from_str(SOURCE);
}

#[test]
fn test_str_literal_assignment()
{
    const SOURCE: &'static str = r##"int main()
    {
        a= "sadsa";
        b="dsa\"sa";
        return 0;
    } "##;
    let mut l = lexer::Lexer::new();
    l.load_from_str(SOURCE);
}

#[test]
fn test_defer()
{
    const SOURCE: &'static str = r##"void main()
    {
        File f;
        if (f = fopen("abc.txt"))
        {
            Qualcosa
        }
        defer {
            fclose(f);
        }
        return 0;
    } "##;
    let mut l = lexer::Lexer::new();
    l.load_from_str(SOURCE);
}