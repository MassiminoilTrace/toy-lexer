use std::char;

#[derive(Debug)]
pub enum Token
{
    Keyword(Keyword),
    Identifier(String),
    Literal(Literal),
    Delimiter(Delimiter),
    Operator(Operator)
}
impl Matches for Token
{
    fn matches(buf: &mut String) -> Option<Self>
    {
        if let Some(token) = Keyword::matches(buf)
        {
            return Some(Self::Keyword(token));
        }
        else if let Some(token) = Literal::matches(buf)
        {
            return Some(Self::Literal(token));
        }
        else if let Some(del) = Delimiter::matches(buf)
        {
            return Some(Self::Delimiter(del));
        }
        else if let Some(operator) = Operator::matches(buf)
        {
            return Some(Self::Operator(operator));
        }
        else if let Some(id) = extract_identifier(buf)
        {
            return Some(Self::Identifier(id));
        }

        *buf = buf.trim_start().to_string();
        return None;
    }
}


#[derive(Debug)]
pub enum Delimiter
{
    Semicolon,
    ParenthesisOpen,
    ParenthesisClosed,
    CurlyBraceOpen,
    CurlyBraceClosed,
}
impl Matches for Delimiter
{
    fn matches(buf: &mut String) -> Option<Self>
    {
        if delimiter_match(buf, ';')
        {
            return Some(Self::Semicolon);
        }
        else if delimiter_match(buf, '(')
        {
            return Some(Self::ParenthesisOpen);
        }
        else if delimiter_match(buf, ')')
        {
            return Some(Self::ParenthesisClosed);
        }
        else if delimiter_match(buf, '{')
        {
            return Some(Self::CurlyBraceOpen);
        }
        else if delimiter_match(buf, '}')
        {
            return Some(Self::CurlyBraceClosed);
        }

        return None;
    }
}

#[derive(Debug)]
pub enum Keyword
{
    If,
    Else,
    Include,
    Return,
    Defer,
    Int
}
impl Matches for Keyword
{
    fn matches(buf: &mut String) -> Option<Self>
    {
        if keyword_match(buf, "if")
        {
            return Some(Keyword::If);
        }
        else if keyword_match(buf, "else")
        {
            return Some(Keyword::Else);
        }
        else if keyword_match(buf, "include")
        {
            return Some(Keyword::Include);
        }
        else if keyword_match(buf, "return")
        {
            return Some(Keyword::Return);
        }
        else if keyword_match(buf, "int")
        {
            return Some(Keyword::Int);
        }
        else if keyword_match(buf, "defer")
        {
            return Some(Keyword::Defer);
        }
        return None;
    }
}

#[derive(Debug)]
pub enum Literal
{
    Integer(String),
    String(String)
}
impl Matches for Literal
{
    fn matches(buf: &mut String) -> Option<Self>
    {
        if buf.len()<=1
        {
            // iterator.all() restituisce true
            // quando la sequenza è vuota, quindi
            // evito che ciò capiti
            return None;
        }
        if buf.chars().take(buf.len()-1).all(|n|{n.is_ascii_digit()})
        {
            if let Some(last_char) = buf.chars().last()
            {
                if !last_char.is_ascii_digit()
                {
                    let num: String = buf.drain(0..buf.len()-1).collect();
                    return Some(Self::Integer(num)); 
                }
            }
        }
        if let Some(first_char) = buf.chars().nth(0)
        {
            if let Some(last_char) = buf.chars().last()
            {
                if let Some(pre_last_char) = buf.chars().nth(buf.len()-2)
                {
                    if first_char=='"' && last_char == '"'
                    && pre_last_char != '\\'
                    {
                        let s = std::mem::take(buf);
                        return Some(Self::String(s));
                    }
                }
            }
        }
        
        return None;
    }
}


#[derive(Debug)]
pub enum Operator
{
    Greater,
    GreaterThan,
    Equals,
    Less,
    LessThan,
    Assignment
}
impl Matches for Operator
{
    fn matches(buf: &mut String) -> Option<Self>
    {
        if buf.len()<2
        {
            return None;
        }
        if let Some(first_char) = buf.chars().nth(0)
        {
            if let Some(second_char) = buf.chars().nth(1)
            {
                return match (first_char, second_char)
                {
                    ('>', '=') => {buf.remove(0);buf.remove(0); return Some(Self::GreaterThan)},
                    ('>', _) => {buf.remove(0);Some(Self::Greater)},
                    ('=', '=') => {buf.remove(0);buf.remove(0);Some(Self::Equals)},
                    ('=', _) => {buf.remove(0);Some(Self::Assignment)},
                    ('<', '=') => {buf.remove(0);buf.remove(0);Some(Self::LessThan)},
                    ('<', _) => {buf.remove(0);Some(Self::Less)},
                    _ => None
                }
            }
        }
        return None;
    }
}



pub struct Lexer
{
    tokens: Vec<Token>
}
impl Lexer
{
    pub fn new()->Self
    {
        Self
        {
            tokens: Vec::default()
        }
    }

    pub fn load_from_str(&mut self, in_data: &str)
    {
        let mut chars = in_data.chars();

        let mut buf: String = String::new();
        while let Some(c) = chars.next()
        {
            buf.push(c);

            if let Some(l) = Token::matches(&mut buf)
            {
                self.tokens.push(l);
            }
        }
        println!("{:#?}", self.tokens);
    }

}

trait Matches where Self:Sized
{
    fn matches(buf: &mut String) -> Option<Self>;
}

fn keyword_match(buffer: &mut String, keyword: &'static str) -> bool
{
    if buffer.starts_with(keyword)
    {
        if let Some(last_char) = buffer.chars().nth(keyword.len())
        {
            if !last_char.is_alphanumeric()
            {
                for _ in 0..keyword.len()
                {
                    buffer.remove(0);
                }
                return true;
            }
        }
    }
    return false;
}

fn extract_identifier(buf: &mut String) -> Option<String>
{
    if let Some(first_char) = buf.chars().nth(0)
    {
        if first_char.is_alphabetic()
        {
            if buf.chars().take(buf.len()-1).all(|e|{e.is_alphanumeric()})
                &&
                !buf.chars().last().unwrap().is_ascii_alphanumeric()
                {
                    return Some(buf.drain(0..buf.len()-1).collect::<String>());
                }
        }
    }
    return None;
}

fn delimiter_match(buf: &mut String, delimiter: char) -> bool
{
    if let Some(first_char) = buf.chars().nth(0)
    {
        if first_char == delimiter
        {
            buf.remove(0);
            return true;
        }
    }
    return false;
}