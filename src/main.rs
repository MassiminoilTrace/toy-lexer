mod lexer;

#[cfg(test)]
mod tests;

fn main() {
    const SOURCE: &'static str = r##"int main()
    {
        return 2;
    }"##;
    let mut l = lexer::Lexer::new();
    l.load_from_str(SOURCE);
}
